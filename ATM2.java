import java.util.Scanner;

 class ATM {
    private static Scanner in;
    private static float balance = 0; 
    private static int anotherTransaction;

    public static void main(String[] args) {
        in = new Scanner(System.in);

        
        System.out.println("Please select your language:");
        System.out.println("1. English");
        System.out.println("2. Spanish");
        int language = in.nextInt();

        System.out.println("Welcome to the ATM!");
        System.out.println("Please enter your account number:");
        int accountNumber = in.nextInt();
        System.out.println("Please enter your PIN:");
        int pin = in.nextInt();

        
        System.out.println("Would you like to create a new account? (Y/N)");
        String createAccount = in.next();
        if (createAccount.equalsIgnoreCase("Y")) {
            System.out.println("Please enter your name:");
            String name = in.next();
            System.out.println("Please enter your initial deposit amount:");
            float initialDeposit = in.nextFloat();
            balance = initialDeposit;
            System.out.println("Account created successfully!");
        }

        // Main menu
        do {
            System.out.println("Please select an option:");
            System.out.println("1. Withdraw");
            System.out.println("2. Deposit");
            System.out.println("3. Transfer");
            System.out.println("4. Change PIN");
            System.out.println("5. Balance Enquiry");
            int option = in.nextInt();

            switch (option) {
                case 1:
                    // Withdraw
                    System.out.println("Please enter amount to withdraw:");
                    float amount = in.nextFloat();
                    if (amount > balance || amount == 0) {
                        System.out.println("Invalid amount!");
                    } else {
                        balance = balance - amount;
                        System.out.println("You have withdrawn " + amount + " dollars. Your new balance is " + balance + " dollars.");
                    }
                    break;

                case 2:
                    // Deposit
                    System.out.println("Please enter amount to deposit:");
                    float depositAmount = in.nextFloat();
                    balance = balance + depositAmount;
                    System.out.println("You have deposited " + depositAmount + " dollars. Your new balance is " + balance + " dollars.");
                    break;

                case 3:
                    // Transfer
                    System.out.println("Please enter amount to transfer:");
                    float transferAmount = in.nextFloat();
                    if (transferAmount > balance || transferAmount == 0) {
                        System.out.println("Invalid amount!");
                    } else {
                        System.out.println("Please enter account number to transfer to:");
                        int transferAccountNumber = in.nextInt();
                        System.out.println("Please enter account holder name:");
                        String transferAccountHolderName = in.next();
                        balance = balance - transferAmount;
                        System.out.println("You have transferred " + transferAmount + " dollars to account number " + transferAccountNumber + " (" + transferAccountHolderName + "). Your new balance is " + balance + " dollars.");
                    }
                    break;

                case 4:
                    // Change PIN
                    System.out.println("Please enter your current PIN:");
                    int currentPIN = in.nextInt();
                    if (currentPIN == pin) {
                        System.out.println("Please enter your new PIN:");
                        int newPIN = in.nextInt();
                        pin = newPIN;
                        System.out.println("PIN changed successfully!");
                    } else {
                        System.out.println("Invalid PIN!");
                    }
                    break;

                case 5:
                    // Balance Enquiry
                    System.out.println("Your balance is " + balance + " dollars.");
                    break;

                default:
                    System.out.println("Invalid option!");
                    break;
            }

            // Another transaction
            System.out.println("Do you want another transaction? (1 = Yes, 2 = No)");
            anotherTransaction = in.nextInt();
        } while (anotherTransaction == 1);

        // Receipt generation
        System.out.println("Thank you for using the ATM!");
        System.out.println("Please take your receipt.");
        System.out.println("Account Number: " + accountNumber);
        System.out.println("Name: " + name);
        System.out.println("Language: " + language);
        System.out.println("Transaction History:");
        // TODO: Print transaction history
    }
}